package com.tcs.ecommerce.configuration;

import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommonSecurityConfiguration {
	@Bean
	public PrincipalExtractor ecommercePrincipalExtractor() {

		return new EcommercePrincipalExtractor();
	}

	@Bean
	public AuthoritiesExtractor ecommerceAuthoritiesExtractor() {

		return new EcommerceAuthoritiesExtractor();
	}
}
