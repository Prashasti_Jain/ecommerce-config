package com.tcs.ecommerce.exception;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

public class EcommerceRestException extends RuntimeException implements Serializable{

	private static final long serialVersionUID = 1L;

	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
	/**
	 * Constructs a new exception.
	 */
	public EcommerceRestException() {
		super();
	}

	/**
	 * Constructs a new exception with the specified detail message.
	 * 
	 * @param message
	 *            the detail message. The detail message is saved for later
	 *            retrieval by the {@link #getMessage()} method.
	 */
	public EcommerceRestException(String message) {
		super(message);
	}
	
	/**
	 * Constructs a new exception with the specified detail message.
	 * 
	 * @param message
	 *            the detail message. The detail message is saved for later
	 *            retrieval by the {@link #getMessage()} method.
	 */
	public EcommerceRestException(String message, HttpStatus httpStatus) {
		super(message);
		this.httpStatus = httpStatus;
	}

	/**
	 * Constructs a new exception with the specified detail cause. .
	 * 
	 * @param cause
	 *            the cause
	 */
	public EcommerceRestException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructs a new exception with the specified detail message and cause. .
	 * 
	 * @param message
	 *            the detail message
	 * @param cause
	 *            the cause
	 */
	public EcommerceRestException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a new exception with the specified detail message, cause,
	 * suppression enabled or disabled, and writable stack trace enabled or
	 * disabled.
	 * 
	 * @param message
	 *            the detail message.
	 * @param cause
	 *            the cause.
	 * @param enableSuppression
	 *            whether or not suppression is enabled or disabled
	 * @param writableStackTrace
	 *            whether or not the stack trace should be writable
	 */
	protected EcommerceRestException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
}
