package com.tcs.ecommerce.model.common;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FaultType {

	protected String type;

	protected String code;

	protected String message;

	protected String developerMessage;

	public FaultType(String type, String code, String message, String developerMessage) {
		super();
		this.type = type;
		this.code = code;
		this.message = message;
		this.developerMessage = developerMessage;
	}
}
