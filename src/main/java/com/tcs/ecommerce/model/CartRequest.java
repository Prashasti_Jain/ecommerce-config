package com.tcs.ecommerce.model;

import lombok.Data;

@Data
public class CartRequest {
	private Integer quantity;
	private Double totalPrice;
	private Integer productId;
	private String username;
}
