package com.tcs.ecommerce.model;

import lombok.Data;

@Data
public class CartResponse {

	private Integer quantity;
	private Integer productId;
	
	
}
