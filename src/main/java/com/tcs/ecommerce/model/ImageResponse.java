package com.tcs.ecommerce.model;

import lombok.Data;

@Data
public class ImageResponse {

	
	private Integer imageId;
	private String imageURL;
	public Integer getImageId() {
		return imageId;
	}
	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	
}
