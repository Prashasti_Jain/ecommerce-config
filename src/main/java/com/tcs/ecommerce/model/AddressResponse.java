package com.tcs.ecommerce.model;


import lombok.Data;

@Data
public class AddressResponse {

	private String firstName;
	private String lastName;
	private String address;
	private String city;
	private String contactNumber;
	
	
	
	
}
