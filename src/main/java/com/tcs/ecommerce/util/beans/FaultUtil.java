package com.tcs.ecommerce.util.beans;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.tcs.ecommerce.model.common.FaultType;

@Component
public class FaultUtil {

	private static final int MAX_LENGTH_TO_FOR_VISIBLE_EXCEPTION_MESSAGE = 300;

	private Logger logger = LoggerFactory.getLogger(getClass());

	FaultUtil() {

	}

	public FaultType createFault(Throwable exception) {
		return createFault(exception, exception.getMessage(), exception.getClass().getSimpleName());
	}

	public FaultType createFault(Throwable ex, String message, String code) {
		logger.error("An error occurred", ex);

		FaultType fault = new FaultType();
		fault.setCode(code);
		fault.setType(ex.getClass().getSimpleName());
		fault.setMessage(message);

		String faultMessage = null;

		if (ex.getClass().isAssignableFrom(SQLException.class)
				|| ex.getMessage() != null && (ex.getMessage().toLowerCase().contains("sql")
						|| ex.getMessage().toLowerCase().contains("hibernate"))) {
			faultMessage = fault.getType();
			fault.setMessage(fault.getType());
		} else {
			faultMessage = ex.getMessage();
			if (faultMessage != null && faultMessage.length() > MAX_LENGTH_TO_FOR_VISIBLE_EXCEPTION_MESSAGE) {
				faultMessage = fault.getType();
			}
		}

		return fault;
	}
}
