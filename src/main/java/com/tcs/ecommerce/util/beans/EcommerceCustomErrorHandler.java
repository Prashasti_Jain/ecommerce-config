package com.tcs.ecommerce.util.beans;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.tcs.ecommerce.exception.EcommerceRestException;
import com.tcs.ecommerce.model.common.FaultType;

@ControllerAdvice
public class EcommerceCustomErrorHandler {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public static final Map<Class<? extends Exception>, HttpStatus> EXCEPTION_TO_HTTP_STATUS_MAP = new HashMap<Class<? extends Exception>, HttpStatus>() {
		private static final long serialVersionUID = 1L;
		{
			put(HttpMessageNotReadableException.class, HttpStatus.BAD_REQUEST);
			put(HttpRequestMethodNotSupportedException.class, HttpStatus.METHOD_NOT_ALLOWED);
			put(MethodArgumentTypeMismatchException.class, HttpStatus.BAD_REQUEST);
		}

	};

	@Autowired
	private FaultUtil faultUtil;

	/**
	 * Handle method error related to method argument validations
	 * 
	 * @param exception
	 * @return FaultType
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public FaultType handleValidationError(MethodArgumentNotValidException exception) {
		logger.error("Invalid argument provided");

		FaultType fault = new FaultType();
		List<String> fieldErrorMessages = new ArrayList<>();

		exception.getBindingResult().getFieldErrors().stream()
				.forEach(error -> fieldErrorMessages.add(error.getField() + ": " + error.getDefaultMessage()));

		fault.setMessage(Arrays.toString(fieldErrorMessages.toArray()));

		return fault;
	}

	/**
	 * Convert Exception into a readable format {@link FaultType}
	 * 
	 * @param exception
	 * @return FaultType
	 */
	@ExceptionHandler({ EcommerceRestException.class, HttpMessageNotReadableException.class,
			// HttpRequestMethodNotSupportedException.class,
			// InvalidDataAccessApiUsageException.class,
			AccessDeniedException.class, MethodArgumentTypeMismatchException.class })
	@ResponseBody
	public ResponseEntity<?> handleCommonRestException(Exception exception) {
		logger.error("REST exception occurred");

		FaultType fault = faultUtil.createFault(exception);
		HttpStatus status = null;

		status = getHttpStatusFromException(exception);

		if (status == null) {
			status = HttpStatus.BAD_REQUEST;
		}

		return new ResponseEntity<FaultType>(fault, status);
	}

	public static HttpStatus getHttpStatusFromException(Exception exception) {
		HttpStatus status;

		if (exception instanceof EcommerceRestException) {
			status = ((EcommerceRestException) exception).getHttpStatus();
		} else {
			status = EXCEPTION_TO_HTTP_STATUS_MAP.get(exception.getClass());
		}

		return status;
	}

	/**
	 * Convert Exception into a readable format {@link FaultType}
	 * 
	 * @param exception
	 * @return FaultType
	 */
	@ExceptionHandler(Throwable.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public FaultType handleException(Throwable exception) {
		logger.error("Unhandeled exception occurred");

		FaultType fault = faultUtil.createFault(exception);

		return fault;
	}
}
